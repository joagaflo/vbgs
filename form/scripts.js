changeDataPolicy = (item) => {
    if (item.checked) $('#form').removeClass("d-none");
    else $('#form').addClass("d-none");
}

loadForm = () => {
    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio",
    });
    $("#formFields").validate({
        rules: {
            age: { required: true },
            gender: { required: true },
            genderIdentify: { required: true },
            genderIdentifyWhich: { required: true },
            sexualOrientation: { required: true },
            sexualOrientationWhich: { required: true },
            ethnic: { required: true },
            estate: { required: true },
            estateStudent: { required: true },
            estateProfessorial: { required: true },
            estateAdministrative: { required: true },
            faculty: { required: true },
            curricularProjectArtes: { required: true },
            curricularProjectCiencias: { required: true },
            curricularProjectIngenieria: { required: true },
            curricularProjectAmbiente: { required: true },
            curricularProjectTecno: { required: true },
            // victim: { required: true },
            genderPerpetrator: { required: true },
            genderPerpetratorWhich: { required: true },
            estatePerpetrator: { required: true },
            estatePerpetratorStudent: { required: true },
            estatePerpetratorProfessorial: { required: true },
            estatePerpetratorAdministrative: { required: true },
            facultyViolence: { required: true },
            physicalSpaceViolence: { required: true },
            dateViolence: { required: true },
            typesViolence: { required: true },
            typesViolenceWhich: { required: true },
            isReported: { required: true },
            isReportedWhich: { required: true },
            preventionProtocol: { required: true },
            accompaniment: { required: true },
            goodAccompaniment: { required: true },
            accompanyingComplaint: { required: true },
            accompanyingComplaintWhich: { required: true },
            attentionPath: { required: true },
            attentionPathWhich: { required: true },
        },
        messages: {
            typesViolence: "Debe elegir al menos una opción"
        }
    });
    // Oculta el ¿Cual? de la pregunta ¿Con cuál género te identificas?
    $('#contGenderIdentifyWhich').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Cuál es tu orientación sexual?
    $('#contSexualOrientationWhich').addClass("d-none");
    // Ocultar proyectos curriculares de facultades
    $('#contCurricularProjectArtes').addClass("d-none");
    $('#contCurricularProjectCiencias').addClass("d-none");
    $('#contCurricularProjectIngenieria').addClass("d-none");
    $('#contCurricularProjectAmbiente').addClass("d-none");
    $('#contCurricularProjectTecno').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Cuál es el género de la persona que ejerció la violencia?
    $('#contGenderPerpetratorWhich').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Qué tipo(s) de violencia(s) fue(ron) ejercida(s)?
    $('#contTypesViolenceWhich').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Esta violencia fue denunciada ante alguna dependencia de la Universidad?
    $('#contIsReportedWhich').addClass("d-none");
    // Pregunta que depende de la respuesta para ¿Activó el Protocolo de Prevención y Atención de Casos de
    // Violencia basada en Género y Violencia Sexual de la Universidad Distrital Francisco José de Caldas?
    $('#contAccompaniment').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Alguno de los colectivos, colectivas, semilleros, Mesas de género 
    // de la Universidad estuvo presente en el proceso de acompañamiento a la denuncia?
    $('#contAccompanyingComplaintWhich').addClass("d-none");
    // Oculta el ¿Cual? de la pregunta ¿Utilizaste alguna ruta de atención y acompañamiento
    // a la denuncia por alguna entidad distrital?
    $('#contAttentionPathWhich').addClass("d-none");
    // Preguntas que dependen de la respuesta para ¿A cuál estamento perteneces?
    $('#contEstateStudent').addClass("d-none");
    $('#contEstateProfessorial').addClass("d-none");
    $('#contEstateAdministrative').addClass("d-none");
    // Preguntas que dependen de la respuesta para ¿A qué estamento pertenece la persona que ejerció la violencia?
    $('#contEstatePerpetratorStudent').addClass("d-none");
    $('#contEstatePerpetratorProfessorial').addClass("d-none");
    $('#contEstatePerpetratorAdministrative').addClass("d-none");

    // this.setTest()
}

setTest = () => {
    $('#age').val("Menor a 18");
    $('#gender').val("Hombre");
    $('#genderIdentify').val("Otro");
    $('#genderIdentifyWhich').val("OGene");
    $('#sexualOrientation').val("Otro");
    $('#sexualOrientationWhich').val("OOrientaSex");
    $('#ethnic').val("Gitano");
    $('#estate').val("Estudiantil");
    $('#faculty').val("Medio Ambiente y Recursos Naturales");
    $('#curricularProjectArtes').val("");
    $('#curricularProjectCiencias').val("");
    $('#curricularProjectIngenieria').val("");
    $('#curricularProjectAmbiente').val("");
    $('#curricularProjectTecno').val("");
    $('#genderPerpetrator').val("Otro");
    $('#genderPerpetratorWhich').val("OGeneMalo");
    $('#estatePerpetrator').val("Profesoral");
    $('#estatePerpetratorStudent').val("");
    $('#estatePerpetratorProfessorial').val("");
    $('#estatePerpetratorAdministrative').val("");
    $('#facultyViolence').val("Otro lugar de la Universidad");
    $('#physicalSpaceViolence').val("Oficinas");
    $('#dateViolence').val("2021-06-23T23:14");
    $('#typesViolence').val("Otro");
    $('#typesViolenceWhich').val("OViolencia");
    $('#isReported').val("Si");
    $('#isReportedWhich').val("Bienestar Institucional");
    $('#preventionProtocol').val("Si");
    $('#accompaniment').val("No");
    $('#goodAccompaniment').val("Si");
    $('#accompanyingComplaint').val("Si");
    $('#accompanyingComplaintWhich').val("ORuta");
    $('#attentionPath').val("Si");
    $('#attentionPathWhich').val("OOrienta");
}

changeGenderIdentify = (value) => {
    if (value === "Otro")
        $('#contGenderIdentifyWhich').removeClass("d-none");
    else {
        $('#contGenderIdentifyWhich').addClass("d-none");
        $('#genderIdentifyWhich').val("");
    }
}

changeSexualOrientation = (value) => {
    if (value === "Otro")
        $('#contSexualOrientationWhich').removeClass("d-none");
    else {
        $('#contSexualOrientationWhich').addClass("d-none");
        $('#sexualOrientationWhich').val("");
    }
}

changeEstate = (value) => {
    $('#contEstateStudent').addClass("d-none");
    $('#estateStudent').val("");
    $('#contEstateProfessorial').addClass("d-none");
    $('#estateProfessorial').val("");
    $('#contEstateAdministrative').addClass("d-none");
    $('#estateAdministrative').val("");

    switch (value) {
        case "Estudiantil":
            $('#contEstateStudent').removeClass("d-none");
            break;
        case "Profesoral":
            $('#contEstateProfessorial').removeClass("d-none");
            break;
        case "Administrativo":
            $('#contEstateAdministrative').removeClass("d-none");
            break;
    }
}

changeFaculty = (value) => {
    $('#contCurricularProjectArtes').addClass("d-none");
    $('#curricularProjectArtes').val("");
    $('#contCurricularProjectCiencias').addClass("d-none");
    $('#curricularProjectCiencias').val("");
    $('#contCurricularProjectIngenieria').addClass("d-none");
    $('#curricularProjectIngenieria').val("");
    $('#contCurricularProjectAmbiente').addClass("d-none");
    $('#curricularProjectAmbiente').val("");
    $('#contCurricularProjectTecno').addClass("d-none");
    $('#curricularProjectTecno').val("");

    switch (value) {
        case "Artes ASAB":
            $('#contCurricularProjectArtes').removeClass("d-none");
            break;
        case "Ciencias y Educación":
            $('#contCurricularProjectCiencias').removeClass("d-none");
            break;
        case "Ingeniería":
            $('#contCurricularProjectIngenieria').removeClass("d-none");
            break;
        case "Medio Ambiente y Recursos Naturales":
            $('#contCurricularProjectAmbiente').removeClass("d-none");
            break;
        case "Tecnológica":
            $('#contCurricularProjectTecno').removeClass("d-none");
            break;
    }
}

changeGenderPerpetrator = (value) => {
    if (value === "Otro")
        $('#contGenderPerpetratorWhich').removeClass("d-none");
    else {
        $('#contGenderPerpetratorWhich').addClass("d-none");
        $('#genderPerpetratorWhich').val("");
    }
}

changeFacultyViolence = (value) => {
    if (value !== "") $('#modalMap').modal('show');
}

changeEstatePerpetrator = (value) => {
    $('#contEstatePerpetratorStudent').addClass("d-none");
    $('#estatePerpetratorStudent').val("");
    $('#contEstatePerpetratorProfessorial').addClass("d-none");
    $('#estatePerpetratorProfessorial').val("");
    $('#contEstatePerpetratorAdministrative').addClass("d-none");
    $('#estatePerpetratorAdministrative').val("");

    switch (value) {
        case "Estudiantil":
            $('#contEstatePerpetratorStudent').removeClass("d-none");
            break;
        case "Profesoral":
            $('#contEstatePerpetratorProfessorial').removeClass("d-none");
            break;
        case "Administrativo":
            $('#contEstatePerpetratorAdministrative').removeClass("d-none");
            break;
    }
}

changeIsReported = (value) => {
    if (value === "Si")
        $('#contIsReportedWhich').removeClass("d-none");
    else {
        $('#contIsReportedWhich').addClass("d-none");
        $('#isReportedWhich').val("");
    }
}

changePreventionProtocol = (value) => {
    if (value === "Si")
        $('#contAccompaniment').removeClass("d-none");
    else {
        $('#contAccompaniment').addClass("d-none");
        $('#accompaniment').val("");
    }
}

changeAccompanyingComplaint = (value) => {
    if (value === "Si")
        $('#contAccompanyingComplaintWhich').removeClass("d-none");
    else {
        $('#contAccompanyingComplaintWhich').addClass("d-none");
        $('#accompanyingComplaintWhich').val("");
    }
}

changeAttentionPath = (value) => {
    if (value === "Si")
        $('#contAttentionPathWhich').removeClass("d-none");
    else {
        $('#contAttentionPathWhich').addClass("d-none");
        $('#attentionPathWhich').val("");
    }
}

changeTypesViolence = (item) => {
    if (item.value === "Otro" && item.checked)
        $('#contTypesViolenceWhich').removeClass("d-none");
    else {
        $('#contTypesViolenceWhich').addClass("d-none");
        $('#typesViolenceWhich').val("");
    }
}