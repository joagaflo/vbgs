require([
    "esri/views/MapView",
    "esri/WebMap",
    "esri/Graphic",
    "esri/widgets/Search",
    "esri/widgets/BasemapGallery",
    "esri/layers/FeatureLayer",
    "esri/geometry/projection",
    "esri/widgets/Expand"
], function (
    MapView,
    WebMap,
    Graphic,
    Search,
    BasemapGallery,
    FeatureLayer,
    projection,
    Expand
) {
    var mapView = new MapView({
        map: new WebMap({
            portalItem: {
                id: "f14ada4ccf2242829de9949a5fdebea0"
            }
        }),
        container: "viewMap",
        center: [-74.12788707688185, 4.6138758562301],
        scale: 50000
    });

    mapView.when(function () {
        console.log("Termino de cargar el mapa")
        // document.getElementById("contLoadingDiv").style.display = "none";
        //Se carga el módulo de proyección de coordenadas
        projection.load();
    }, function (error) {
        console.log("Error cargando el mapa: ", error);
        // document.getElementById("contLoadingDiv").style.display = "none";
    });

    // Barra de busqueda
    var searchWidget = new Search({ view: mapView });
    mapView.ui.add(searchWidget, { position: "top-right" });

    // Galeria de mapas base
    var basemapGallery = new BasemapGallery({ view: mapView });
    const bgExpand = new Expand({
        view: mapView,
        content: basemapGallery
    });
    mapView.ui.add(bgExpand, { position: "bottom-left" });

    // Acción al hacer clic en el mapa
    mapView.on("click", function (event) {
        createPointGraphic([event.mapPoint.longitude, event.mapPoint.latitude]);
    });

    /**
     * @author José Andrés García Flórez
     * @since 21/4/2021
     * Crear el punto y lo agrega a la capa de gráficos de la escena
     * @param {JSON} coordinates Coordenadas  xy
     */
    createPointGraphic = function (coordinates) {
        mapView.graphics.removeAll(); // Limpia el punto anterior

        this.point = {
            type: "point",
            x: coordinates[0],
            y: coordinates[1]
        };

        let markerSymbol = {
            type: "picture-marker",
            url: "../images/point-pin.png",
            width: "32px",
            height: "32px",
            yoffset: "16px"
        };

        let pointGraphic = new Graphic({
            geometry: this.point,
            symbol: markerSymbol
        });

        // Agrega el punto al mapa
        mapView.graphics.add(pointGraphic);
        return point;
    }

    window.applyEditsToIncidents = function (params) {
        // Sub estamento ¿A cuál estamento perteneces?
        switch (true) {
            case $('#estateStudent').val() !== "":
                params.subEstate = $('#estateStudent').val()
                break;
            case $('#estateProfessorial').val() !== "":
                params.subEstate = $('#estateProfessorial').val()
                break;
            case $('#estateAdministrative').val() !== "":
                params.subEstate = $('#estateAdministrative').val()
                break;
        }

        // ¿Cuál es tu proyecto curricular?
        switch (true) {
            case $('#curricularProjectArtes').val() !== "":
                params.curricularProject = $('#curricularProjectArtes').val()
                break;
            case $('#curricularProjectCiencias').val() !== "":
                params.curricularProject = $('#curricularProjectCiencias').val()
                break;
            case $('#curricularProjectIngenieria').val() !== "":
                params.curricularProject = $('#curricularProjectIngenieria').val()
                break;
            case $('#curricularProjectAmbiente').val() !== "":
                params.curricularProject = $('#curricularProjectAmbiente').val()
                break;
            case $('#curricularProjectTecno').val() !== "":
                params.curricularProject = $('#curricularProjectTecno').val()
                break;
        }

        // Sub estamento ¿A qué estamento pertenece la persona que ejerció la violencia?
        switch (true) {
            case $('#estatePerpetratorStudent').val() !== "":
                params.subEstatePerpetrator = $('#estatePerpetratorStudent').val()
                break;
            case $('#estatePerpetratorProfessorial').val() !== "":
                params.subEstatePerpetrator = $('#estatePerpetratorProfessorial').val()
                break;
            case $('#estatePerpetratorAdministrative').val() !== "":
                params.subEstatePerpetrator = $('#estatePerpetratorAdministrative').val()
                break;
        }

        // Tipos de violencia ¿Qué tipo(s) de violencia(s) fue(ron) ejercida(s)? (Puedes elegir múltiples opciones)
        let typesViolences = ""
        $("input:checkbox[name=typesViolence]:checked").each(function () {
            typesViolences += $(this).val() + ", ";
        });
        typesViolences = typesViolences.slice(0, -2)

        const attributesParams = {
            edad: params.age,
            sexo: params.gender,
            genero: params.genderIdentify,
            genero_otro: params.genderIdentifyWhich,
            orientacion_sexual: params.sexualOrientation,
            orientacion_sexual_otro: params.sexualOrientationWhich,
            etnia: params.ethnic,
            denunciante_estamento: params.estate,
            denunciante_subestamento: params.subEstate,
            denunciante_facultad: params.faculty,
            denunciante_proyecto: params.curricularProject,
            acusado_genero: params.genderPerpetrator,
            acusado_genero_otro: params.genderPerpetratorWhich,
            acusado_estamento: params.estatePerpetrator,
            acusado_subestamento: params.subEstatePerpetrator,
            acusado_facultad: params.facultyViolence,
            lugar_violencia: params.physicalSpaceViolence,
            fecha_violencia: Date.parse(params.dateViolence),
            tipos_violencias: typesViolences,
            tipos_violencias_otro: params.typesViolenceWhich,
            denuncia_ud: params.isReported,
            denuncia_ud_cual: params.isReportedWhich,
            protocolo_ud: params.preventionProtocol,
            protocolo_ud_acompanamiento: params.accompaniment,
            acompanamiento_satisfactorio: params.goodAccompaniment,
            colectivo_presente: params.accompanyingComplaint,
            colectivo_presente_cual: params.accompanyingComplaintWhich,
            ruta_atencion: params.attentionPath,
            ruta_atencion_cual: params.attentionPathWhich,
            x: this.point.x,
            y: this.point.y
        }

        const featureLayer = new FeatureLayer({
            url: "https://services5.arcgis.com/MufcvaItL5oCCobA/arcgis/rest/services/bd_vbg_ipazud/FeatureServer/0",
            outFields: ["*"],
            id: "bd_vbg_ipazud"
        });

        const editFeature = new Graphic({
            geometry: this.point,
            attributes: attributesParams
        });

        featureLayer
            .applyEdits({ addFeatures: [editFeature] })
            .then((editsResult) => {
                $('#modalSave').modal('show');
                console.log("RESULTADO EXITO:", editsResult)
            }).catch((error) => {
                console.log("RESULTADO ERROR:", error);
            });
    }
});

validateForm = () => {
    if ($("#formFields").valid()) {
        applyEditsToIncidents($('#formFields').serializeArray().reduce((function (acc, val) {
            acc[val.name.replace('[', '_').replace(']', '')] = val.value;
            return acc;
        }), {}));
    } else {
        $('#modalNoData').modal('show');
    }
}